#include <cstdlib>
#include <cstring>
#include <iostream>
#include "../library/shell.h"
#include "../library/command.h"
using namespace std;
using namespace mshconsole;

static void setup(Shell *);

int cdExecute(CommandExecutor* whoExecuted,const Command::Datas& data, const vector<const char*> argv){
    if (argv.size() == 0) {
        cerr << "expected argument to \"cd\"\n";
    } else {
        int ret;
        if (strcmp("~",argv[0])==0) {
            ret = chdir(getenv("HOME"));
        }
        else {
            ret = chdir(argv[0]);
        }
        if (ret != 0) {
            cerr << "cd: error executing the command\n";
            return ret;
        }
    }
    return 0;
}

int exitExecute(CommandExecutor* whoExecuted,const Command::Datas& data, const vector<const char*> argv){
    whoExecuted->exit();
    return 0;
}

int helpExecute(CommandExecutor* whoExecuted,const Command::Datas& data, const vector<const char*> argv){
   cout << data.getOptData('p')->getString() << "\n";
    if(data.getOptData('h')->getInt()){
        cout << "These are the further details." << endl;
        return 0;
    }
    if(strcmp(data.getOptData('p')->getString(),"prova")==0){
        cout << "prova\n";
        return 0;
    }
    cout << "Snippet console generated with msh-console library.\nUse option -h for further details." << endl;
    return 0;
}

/**
  Little snippet for library use.
*/

int main(int argc, char **argv)
{
    string c = "msh-console-test";
    string ps = "[msh-console-test]:";
    Shell mshConsoleTest(c, ps, cin,cout,cerr, &setup);
    Command help("help", &helpExecute);
    struct poptOption h = {"help",'h',POPT_ARG_NONE,NULL,1000,"advance","advaced options"};
    struct poptOption p = {"prova",'p',POPT_ARG_STRING,NULL,1000,"cose","cosecose"};
    help.addOption(h);
    help.addOption(p);

    Command exit("exit", &exitExecute);
    Command cd("cd", &cdExecute);

    //add builtin commands
    mshConsoleTest.addCmd(&help, true);
    mshConsoleTest.addCmd(&cd);
    mshConsoleTest.addCmd(&exit);
    mshConsoleTest.launch();

    return EXIT_SUCCESS;
}

void setup(Shell *s){
    cout << "Now entering in test shell...\n" << endl;
    //s->executeCmd("stty -ctlecho");
}
