/**
    MIT License

    Copyright (c) 2016 Claudio Maggioni

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include "command.h"

namespace mshconsole{
    inline const union Command::Data::Arg& Command::Data::getArg() const{
        return d;
    }
    enum Command::Data::Types Command::Data::getType() const{
        if(of->argInfo & POPT_ARG_INT || of->argInfo & POPT_ARG_VAL || of->argInfo == POPT_ARG_NONE){
            return INT;
        }
        else if(of->argInfo & POPT_ARG_STRING){
            return STRING;
        }
        else if(of->argInfo & POPT_ARG_LONG){
            return LONG;
        }
        else if(of->argInfo & POPT_ARG_FLOAT){
            return FLOAT;
        }
        else if(of->argInfo & POPT_ARG_DOUBLE){
            return DOUBLE;
        }
        //else return NULL;
    }
    int Command::Data::getInt() const{
        return d.i;
    }
    char* Command::Data::getString() const{
        return d.i ? d.s : const_cast<char*>("");
    }
    long Command::Data::getLong() const{
        return d.l;
    }
    float Command::Data::getFloat() const{
        return d.f;
    }
    double Command::Data::getDouble() const{
        return d.d;
    }
}
