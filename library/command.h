/**
    MIT License

    Copyright (c) 2016 Claudio Maggioni

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#ifndef COMMAND_H
#define COMMAND_H
#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <unistd.h>
extern "C"{
    #include <popt.h>
}
#include "commandexecutor.h"

namespace mshconsole{
    class Command
    {
        const std::string name;
        void checkObj();
        std::vector<poptOption> opts;
        static const struct poptOption POPT_TERMINATOR;
        size_t optionNum;

    public:
        class Data{
        public:
            union Arg{
                int i;
                char* s;
                long l;
                float f;
                double d;
            };
            enum Types{
                STRING,
                INT,
                LONG,
                FLOAT,
                DOUBLE
            };

        private:
            union Arg d;
            poptOption* of;
            Data(poptOption* o) : of(o), d() {}
            friend class mshconsole::Command;

        public:
            const union Arg& getArg() const;
            enum Types getType() const;
            int getInt() const;
            char* getString() const;
            long getLong() const;
            float getFloat() const;
            double getDouble() const;
        };

        class Datas : public std::vector<Data*>{
        public:
            Data* getOptData(const std::string& optionName) const;
            Data* getOptData(char opt) const;
            Data* operator[](const std::string& opt) const;
        };

        class DuplicatedOptionException{};
        class CommandNameNotValidException{};
        class InvalidOptionException{};

        Command(const std::string& n, int (*funcptr)(CommandExecutor* whoExecuted,const Datas& data, const std::vector<const char*> argv));
        Command(const Command&);
        ~Command();
        const std::string& getName();
        int execute(const struct Params&, CommandExecutor*,std::ostream& errorStream=std::cerr);
        void addOption(const poptOption& option);

    private:
        int (*funcCommand)(CommandExecutor* whoExecuted,const Datas& data, const std::vector<const char*> argv);
    };
}

#endif // COMMAND_H
