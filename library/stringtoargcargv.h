/** stringtoargcargv.cpp -- Parsing a string to std::vector<string>

  Copyright (C) 2011 Bernhard Eder

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Bernhard Eder blog_at_bbgen.net

*/

#ifndef STRINGTOARGCARGV_H
#define STRINGTOARGCARGV_H
#pragma once

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>
#include <string>

#include <cstdlib>
#include <cstring>

namespace StringToArgcArgv{
    void vectorToArgcArgv(const std::vector<std::string>& args, int* argc, char*** argv);
    std::vector<std::string> parseStringToVector(const std::string& args);
    void stringToArgcArgv(const std::string& str, int* argc, char*** argv);
    void freeString(int& argc, char**& argv);
}
#endif //STRINGTOARGCARGV_H
