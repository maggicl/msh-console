/**
    MIT License

    Copyright (c) 2016 Claudio Maggioni

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#include "shell.h"
namespace mshconsole {
    Shell::Commands::Commands(Shell* s) : commands(), threadCommands()
    {
        parent = s;
    }

    void Shell::Commands::add(Command* cmd, bool isthread){
        if(!isthread) commands.push_back(cmd);
        else threadCommands.push_back(cmd);
    }

    size_t Shell::Commands::howMany() const{
        return commands.size();
    }

    int Shell::Commands::launch(const struct Params& p, bool launchThread){
        for(unsigned int i=0; i<(launchThread ? threadCommands.size() : commands.size()); i++){
            if((launchThread ? threadCommands[i]->getName() : commands[i]->getName())==p.argv[0]){
                return (launchThread ? threadCommands[i]->execute(p, parent) : commands[i]->execute(p, parent));
            }
        }
        throw CommandNotFoundException();
    }
    bool Shell::Commands::found(const std::string& s, bool launchThread){
        for(unsigned int i=0; i<(launchThread ? threadCommands.size() : commands.size()); i++){
            if((launchThread ? threadCommands[i]->getName() : commands[i]->getName())==s){
                return true;
            }
        }
        return false;
    }
}


