/**
    MIT License

    Copyright (c) 2016 Claudio Maggioni

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

#ifndef SHELL_H
#define SHELL_H
#pragma once

#include <iostream>
#include <sys/wait.h>
#include <unistd.h>
#include <string>
#include <vector>
#include <cstddef>
#include <csignal>
#include <cerrno>
#include "command.h"
#include <wordexp.h>
#include "commandexecutor.h"
#include "stringtoargcargv.h"
using namespace StringToArgcArgv;

namespace mshconsole {

    class Shell : public CommandExecutor
    {
        class Commands
        {
            Shell* parent;
            std::vector<Command*> commands; //commands that work the same thread of the shell
            std::vector<Command*> threadCommands; //commands that work on a different thread

        public:
            Commands(Shell*);
            void add(Command*, bool isthread=false);
            size_t howMany() const;
            int launch(const struct Params& args, bool launchThread=false);
            bool found(const std::string& args, bool launchThread=false);
        };

        static bool SIGINTRaised;
        Commands cmds;
        std::string ps;
        std::string name;
        std::ostream& errorStream;
        std::ostream& outputStream;
        std::istream& inputStream;
        void (*shellSetup)(Shell *);
        void (*shellPostSetup)(Shell *);

        int launchCmd(const struct Params& args);
        static void SIGINTHandler(int,siginfo_t *info, void *);
        std::string* readLine();
        std::vector<std::string> splitLine(const std::string* line);
        int executePipe(std::vector<std::string>& argv);
        void deleteParams(struct Params p);

        class IsUndoingLineException {};
        class CommandNotFoundException {};

    public:
        Shell(std::string n, std::string ps, std::istream& inputStream, std::ostream& outputStream,
              std::ostream& errorStream, void (*s)(Shell*)=0, void (*pss)(Shell*)=0);
        const std::string& getPs() const;
        void setPs(const std::string &value);
        const std::string& getName() const;
        void setName(const std::string &value);
        int launch();
        void setShellSetup(void (*)(Shell *));
        void (*getShellSetup())(Shell*) const;
        void setShellPostSetup(void (*)(Shell *));
        void (*getShellPostSetup())(Shell*) const;

        //for in-shell commands
        void addCmd(Command *cmd, bool isThread=false);

        int executeCmd(const struct Params& args, bool isPipe=false);
        int executeCmd(const std::string& args);
        size_t howManyCmds() const;
    };

    extern struct sigaction* setHandler(unsigned signal, void (*funcptr)(int, siginfo_t*, void*));
    extern void restoreHandler(unsigned signal, struct sigaction* before);
}

#endif // SHELL_H
