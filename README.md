<!-- vim: set et ts=2 sw=2 tw=80 : -->

# msh-console

Library that provides a bash-like command line interface for C++ programs.

## Abandoned

This was my first OOP project, so pardon the style and the conventions taken.
This project is currently unmantained.

## Features include:

* flag handling implemented with `popt.h`;
* creation of personalized commands in the same thread of the shell or in other 
threads;
* basic shell functionalities, such as execution of other programs in the
system. This is implemented with `execvp()`.
* piping, implemented with `<unistd.h>` `pipe()` function. Code based on
https://stackoverflow.com/a/5207730.

## What is not actually included:

* advanced shell features, like:
  * redirection,
  * scripting,
  * command history.

## Credits

Code based on
["Write a Shell in C"](http://brennan.io/2015/01/16/write-a-shell-in-c/)
tutorial by Stephen Brennan. The code contains `stringtoargcargv.cpp`, a set of
functions written by Bernhard Eder
[here](http://web.archive.org/web/20121030075237/http://bbgen.net/blog/2011/06/string-to-argc-argv)
for parsing a string into `argc` and `argv`.  

## Compiling

The library can be compiled as shared library with the CMakeLists.txt file
already in `msh-console-library/`.
